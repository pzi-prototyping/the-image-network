from pylab import *
from numpy import *
from PIL import Image

from PCV.localdescriptors import sift
# from PCV.tools import imtools
import pydot, json
import sys, os

"""
This is the example graph illustration of matching images from Figure 2-10.
To download the images, see ch2_download_panoramio.py.
"""



path = sys.argv[1]
# path = os.path.abspath(download_path)

# list of downloaded filenames
# imlist = imtools.get_imlist(download_path)
from glob import glob
imlist = glob(os.path.join(path, "*.jpg"))
nbr_images = len(imlist)

matchscores = load(os.path.join(path, "matchscores.dump"))
threshold = 2 # min number of matches needed to create link
g = pydot.Dot(graph_type='graph') # don't want the default directed graph 

nodes = {}
links = []

def thumb_for (im, w=100, h=100):
    b = os.path.basename(im)
    b = os.path.join(path, "thumbs", os.path.splitext(b)[0] + ".thumb.png")
    if not os.path.exists(b):
        im = Image.open(im)
        im.thumbnail((w,h))
        im.save(b) 
    return b

thumbpath = os.path.join(path, "thumbs")
try:
    os.mkdir(thumbpath)
except OSError:
    pass

for i in range(nbr_images):
    for j in range(i+1,nbr_images):
        if matchscores[i,j] > threshold:
            # first image in pair
            ithumb = thumb_for(imlist[i])
            g.add_node(pydot.Node(str(i),fontcolor='transparent',shape='rectangle',image=os.path.abspath(ithumb)))
            nodes[i] = imlist[i]

            # second image in pair
            jthumb = thumb_for(imlist[j])
            g.add_node(pydot.Node(str(j),fontcolor='transparent',shape='rectangle',image=os.path.abspath(jthumb)))
            nodes[j] = imlist[j]

            g.add_edge(pydot.Edge(str(i),str(j)))
            links.append((imlist[i], imlist[j]))

def wrap_thumb (p):
    t = thumb_for(p)
    ret = {}
    ret['path'] = os.path.relpath(t, path)
    im = Image.open(t)
    ret['width'] = im.size[0]
    ret['height'] = im.size[1]
    return ret

# g.write_dot(os.path.join(path, 'graph.dot'))
g.write_png(os.path.join(path, 'graph.png'))
with open(os.path.join(path, "graph.json"), "w") as f:
    nodes = nodes.values()
    nodes.sort()
    links = [{'source': nodes.index(x[0]), 'target': nodes.index(x[1])} for x in links]
    nodes = [{
        'path': os.path.relpath(x, path),
        'thumb': wrap_thumb(x)
    } for x in nodes]
    json.dump({'nodes': nodes, 'links': links}, f)

# copy graph.html into path
os.system('cp graph.html "{0}"'.format(path))
